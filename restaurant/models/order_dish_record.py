from django.db import models


class OrderDishRecord(models.Model):
    order = models.ForeignKey('restaurant.Order', on_delete=models.CASCADE)
    dish = models.ForeignKey('restaurant.Dish', verbose_name='Блюдо', on_delete=models.RESTRICT)
    modificators = models.ManyToManyField('restaurant.Modificator', verbose_name='Модификаторы', blank=True)

    class Meta:
        verbose_name = "Запись о позиции"
        verbose_name_plural = "Записи о позициях"

    def __str__(self):
        return f"{self.order} {self.dish}"
