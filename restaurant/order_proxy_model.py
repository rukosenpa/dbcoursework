from restaurant.models import Order


class OrderSummary(Order):
    class Meta:
        proxy = True
        verbose_name = 'Выручка по датам'
        verbose_name_plural = 'Выручка по датам'
