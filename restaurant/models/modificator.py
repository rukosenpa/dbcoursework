from django.db import models


class Modificator(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50)
    price = models.DecimalField(verbose_name='Цена', max_digits=7, decimal_places=2)
    group = models.ForeignKey('restaurant.Group', related_name='modificators', verbose_name='Группа', null=True,
                              blank=True, on_delete=models.RESTRICT)

    class Meta:
        verbose_name = "Модификатор"
        verbose_name_plural = "Модификаторы"

    def __str__(self):
        return f"{self.title} ({self.group.title if self.group else 'Общий'})"
