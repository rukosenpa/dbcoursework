from django.db import models


class Order(models.Model):
    created_at = models.DateTimeField(verbose_name="Дата заказа")
    total = models.DecimalField(verbose_name="Сумма заказа", default=0, max_digits=10, decimal_places=2)
    dishes = models.ManyToManyField('restaurant.Dish', through='restaurant.OrderDishRecord', verbose_name='Блюда')

    class Meta:
        verbose_name = "Заказ"
        verbose_name_plural = "Заказы"

    def __str__(self):
        return f"#{self.id} от {self.created_at}"
