from django.db import models


class ModificatorLayout(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50)
    modificators = models.ManyToManyField('restaurant.Modificator',
                                          through='restaurant.ModificatorLayoutRecord')

    class Meta:
        verbose_name = "Схема модификаторов"
        verbose_name_plural = "Схемы модификаторов"

    def __str__(self):
        return self.title
