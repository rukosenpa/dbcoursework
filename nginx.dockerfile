FROM nginx:1.13

RUN rm -v /etc/nginx/nginx.conf
COPY nginx.conf /etc/nginx/
COPY favicon.ico /nginx/static/
