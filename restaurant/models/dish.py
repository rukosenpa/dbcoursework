from django.db import models


class Dish(models.Model):
    title = models.CharField(verbose_name='Название', max_length=50)
    price = models.DecimalField(verbose_name='Цена', max_digits=7, decimal_places=2)
    menu_position = models.IntegerField(verbose_name='Позиция в меню', default=1)

    category = models.ForeignKey('restaurant.Category', verbose_name='Категория', on_delete=models.RESTRICT)
    group = models.ForeignKey('restaurant.Group', verbose_name='Группа', on_delete=models.RESTRICT, blank=True,
                              null=True)

    modificator_layout = models.ForeignKey('restaurant.ModificatorLayout', verbose_name='Схема модификаторов',
                                           on_delete=models.RESTRICT, blank=True, null=True)

    modificators = models.ManyToManyField('restaurant.Modificator', through='restaurant.ModificatorDishRecord',
                                          verbose_name='Модификаторы', blank=True)

    class Meta:
        verbose_name = "Блюдо"
        verbose_name_plural = "Блюда"

    def __str__(self):
        return self.title
