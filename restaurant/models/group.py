from django.db import models


class Group(models.Model):
    title = models.CharField(verbose_name="Название", max_length=50)

    class Meta:
        verbose_name = "Группа"
        verbose_name_plural = "Группы"

    def __str__(self):
        return self.title
