from django.contrib import admin
from django.db.models import Sum

from restaurant.models import *
from restaurant.order_proxy_model import OrderSummary


@admin.register(Category)
class CategoryModelAdmin(admin.ModelAdmin):
    ...


@admin.register(Group)
class GroupModelAdmin(admin.ModelAdmin):
    ...


@admin.register(Modificator)
class ModificatorModelAdmin(admin.ModelAdmin):
    ...


class ModificatorLayoutRecord(admin.TabularInline):
    model = ModificatorLayoutRecord


@admin.register(ModificatorLayout)
class ModificatorLayoutModelAdmin(admin.ModelAdmin):
    inlines = [
        ModificatorLayoutRecord
    ]


class ModificatorRecordInline(admin.TabularInline):
    model = ModificatorDishRecord


@admin.register(Dish)
class DishModelAdmin(admin.ModelAdmin):
    inlines = [
        ModificatorRecordInline
    ]

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return []
        return super().get_inline_instances(request, obj)


class DishRecordInline(admin.StackedInline):
    model = OrderDishRecord
    filter_vertical = ('modificators',)


@admin.register(Order)
class OrderModelAdmin(admin.ModelAdmin):
    exclude = ("total",)
    inlines = [
        DishRecordInline
    ]


@admin.register(OrderSummary)
class OrdersSummaryAdmin(admin.ModelAdmin):
    change_list_template = 'order_summary.html'

    def changelist_view(self, request, extra_context=None):
        response = super().changelist_view(
            request,
            extra_context=extra_context,
        )

        try:
            qs = response.context_data['cl'].queryset
        except (AttributeError, KeyError):
            return response

        response.context_data['summary'] = qs.values(
            'created_at__year', 'created_at__month'
        ).annotate(
            total_month=Sum('total')
        ).order_by(
            'created_at__year'
        )

        response.context_data['summary_total'] = qs.aggregate(total_sales=Sum('total'))
        return response

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
