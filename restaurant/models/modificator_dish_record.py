from django.db import models


class ModificatorDishRecord(models.Model):
    modificator = models.ForeignKey('restaurant.Modificator', related_name='dish_records', verbose_name='Модификатор',
                                    on_delete=models.CASCADE)
    dish = models.ForeignKey('restaurant.Dish', related_name='modificators_records', verbose_name='Блюдо',
                             on_delete=models.CASCADE)

    default_value = models.IntegerField(verbose_name='кол-во по умолчанию')
    min_value = models.IntegerField(verbose_name='Минимальное кол-во')
    max_value = models.IntegerField(verbose_name='Максимальное кол-во', null=True, blank=True)
    free_value = models.IntegerField(verbose_name='Бесплатное кол-во')

    class Meta:
        verbose_name = "Запись значений модификатора блюда"
        verbose_name_plural = "Записи значений модификаторов блюд"

    def __str__(self):
        return str(self.modificator) + " " + str(self.dish)
