from django.db import models


class ModificatorLayoutRecord(models.Model):
    layout = models.ForeignKey('restaurant.ModificatorLayout', related_name='values', on_delete=models.CASCADE)
    modificator = models.ForeignKey('restaurant.Modificator', verbose_name='Модификатор', on_delete=models.CASCADE)

    default_value = models.IntegerField(verbose_name='кол-во по умолчанию', default=0)
    min_value = models.IntegerField(verbose_name='Минимальное кол-во', default=0)
    max_value = models.IntegerField(verbose_name='Максимальное кол-во', null=True, blank=True)
    free_value = models.IntegerField(verbose_name='Бесплатное кол-во', default=0)

    class Meta:
        verbose_name = "Запись значений схемы модификатора"
        verbose_name_plural = "Записи значений схемы модификатора"

    def __str__(self):
        return str(self.layout) + " - " + str(self.modificator)
